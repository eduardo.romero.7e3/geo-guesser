package cat.itb.catitbgeoguesser;

public class QuestionModel {
    private int question, answerA, answerB, answerC, answerD, hint;
    private int correct;

    public QuestionModel(int question, int answerA, int answerB, int answerC, int answerD, int correct, int hint) {
        this.question = question;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.correct = correct;
        this.hint = hint;
    }

    public int getQuestion() {
        return question;
    }

    public int getAnswerA() {
        return answerA;
    }

    public int getAnswerB() {
        return answerB;
    }

    public int getAnswerC() {
        return answerC;
    }

    public int getAnswerD() {
        return answerD;
    }

    public int getCorrect() {
        return correct;
    }

    public int getHint(){
        return hint;
    }

}
