package cat.itb.catitbgeoguesser;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class QuestionViewModel extends ViewModel {
    private QuestionModel[] questionSet = {
            new QuestionModel(R.string.question1, R.string.q1A, R.string.q1B, R.string.q1C, R.string.q1D, 2, R.string.hint1),
            new QuestionModel(R.string.question2, R.string.q2A, R.string.q2B, R.string.q2C, R.string.q2D, 2, R.string.hint2),
            new QuestionModel(R.string.question3, R.string.q3A, R.string.q3B, R.string.q3C, R.string.q3D, 0, R.string.hint3),
            new QuestionModel(R.string.question4, R.string.q4A, R.string.q4B, R.string.q4C, R.string.q4D, 1, R.string.hint4),
            new QuestionModel(R.string.question5, R.string.q5A, R.string.q5B, R.string.q5C, R.string.q5D, 2, R.string.hint5),
            new QuestionModel(R.string.question6, R.string.q6A, R.string.q6B, R.string.q6C, R.string.q6D, 0, R.string.hint6),
            new QuestionModel(R.string.question7, R.string.q7A, R.string.q7B, R.string.q7C, R.string.q7D, 0, R.string.hint7),
            new QuestionModel(R.string.question8, R.string.q8A, R.string.q8B, R.string.q8C, R.string.q8D, 2, R.string.hint8),
            new QuestionModel(R.string.question9, R.string.q9A, R.string.q9B, R.string.q9C, R.string.q9D, 1, R.string.hint9),
            new QuestionModel(R.string.question10, R.string.q10A, R.string.q10B, R.string.q10C, R.string.q10D, 3, R.string.hint10)
    };
    private int index = 0;
    private double correctCount = 0;
    private int hintsRemaining;
    private boolean init = true;

    public QuestionViewModel() {
    }

    public void randomize(){
        ArrayList<QuestionModel> list = new ArrayList<QuestionModel>(Arrays.asList(questionSet));
        Collections.shuffle(list);
        list.toArray(questionSet);
        index = 0;
        correctCount = 0;
        hintsRemaining = 3;
    }

    public QuestionModel getCurrentQuestion(){
        return questionSet[index];
    }

    public void nextQuestion(){
        index++;
    }

    public int getQuestionNumber(){
        return index + 1;
    }

    public double getCorrectCount() {
        return correctCount;
    }

    public void addCorrect(){
        correctCount++;
    }

    public void substractCorrect(){
        correctCount = correctCount - 0.5;
    }

    public void oneLessHint(){
        hintsRemaining--;
    }

    public int getHintsRemaining(){
        return hintsRemaining;
    }

    public boolean isInit(){
        return init;
    }

    public void setInit(boolean init){
        this.init = init;
    }

}