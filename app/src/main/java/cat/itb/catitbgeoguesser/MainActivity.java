package cat.itb.catitbgeoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button[] buttons;
    TextView questionView;
    ProgressBar questionProgress;
    Button hintButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionView = findViewById(R.id.questionView);
        questionProgress = findViewById(R.id.questionProgress);
        hintButton = findViewById(R.id.hintButton);

        buttons = new Button[]{
                findViewById(R.id.answer1), findViewById(R.id.answer2),
                findViewById(R.id.answer3), findViewById(R.id.answer4)};

        final QuestionViewModel q = new ViewModelProvider(this).get(QuestionViewModel.class);
        if(q.isInit()) q.randomize();
        q.setInit(false);

        setHintAmount(hintButton, q);
        showQuestion(buttons, q);

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.dialogTitle);


        setTouch(buttons, q, dialog);

        dialog.setPositiveButton(R.string.positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                q.randomize();
                setHintAmount(hintButton, q);
                showQuestion(buttons, q);
            }
        });

        dialog.setNegativeButton(R.string.negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                System.exit(0);
            }
        });

        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(q.getHintsRemaining() > 0){
                    q.oneLessHint();
                    setHintAmount(hintButton, q);
                    Toast.makeText(getApplicationContext(), getText(q.getCurrentQuestion().getHint()), Toast.LENGTH_SHORT).show();
                    hintButton.setEnabled(false);
                }

            }
        });

    }


    public void highlight(Button b) {
        b.setBackgroundColor(getResources().getColor(R.color.press));
        b.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    public void highlightAnswer(final Button b, final QuestionViewModel q, boolean correct, final AlertDialog.Builder dialog) {
        if (correct) {
            b.setBackgroundColor(getResources().getColor(R.color.right));
        } else {
            b.setBackgroundColor(getResources().getColor(R.color.wrong));
        }
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setEnabled(false);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                b.setBackgroundColor(getResources().getColor(R.color.answer));
                b.setTextColor(getResources().getColor(R.color.text));
                if (q.getQuestionNumber() < 10) {
                    q.nextQuestion();
                    showQuestion(buttons, q);
                }
                else {
                    StringBuilder finalMessageSBuilder = new StringBuilder();
                    finalMessageSBuilder.append("You got ").append(q.getCorrectCount()).append(" points out of 10! Wanna try again?");
                    dialog.setMessage(finalMessageSBuilder.toString());
                    dialog.show();
                }

                for (int i = 0; i < buttons.length; i++) {
                    buttons[i].setEnabled(true);
                }
            }
        }, 1000);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setTouch(final Button[] buttons, final QuestionViewModel q, final AlertDialog.Builder dialog) {
        for (int i = 0; i < buttons.length; i++) {
            final int finalI = i;
            buttons[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            highlight(buttons[finalI]);
                            return true;
                        case MotionEvent.ACTION_UP:
                            if (q.getCurrentQuestion().getCorrect() == finalI) {
                                highlightAnswer(buttons[finalI], q, true, dialog);
                                q.addCorrect();
                            } else {
                                highlightAnswer(buttons[finalI], q, false, dialog);
                                q.substractCorrect();
                            }
                            return true;
                    }
                    return false;
                }
            });
        }
    }

    public void showQuestion(Button[] buttons, QuestionViewModel q) {
        questionView.setText(q.getCurrentQuestion().getQuestion());
        buttons[0].setText(q.getCurrentQuestion().getAnswerA());
        buttons[1].setText(q.getCurrentQuestion().getAnswerB());
        buttons[2].setText(q.getCurrentQuestion().getAnswerC());
        buttons[3].setText(q.getCurrentQuestion().getAnswerD());

        questionProgress.setProgress(q.getQuestionNumber());
        if(q.getHintsRemaining() > 0) hintButton.setEnabled(true);
    }

    public void setHintAmount(Button b, QuestionViewModel q){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(q.getHintsRemaining()).append("/3 hints remaining");
        b.setText(stringBuilder.toString());
    }


}

/**
 * TODO: Actual questions
 * TODO: End message
 */

